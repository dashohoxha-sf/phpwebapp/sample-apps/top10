<?php
/* This file is part of phpWebApp. */

/**
 * This file is included every time. It contains functions,
 * constants and variables that are used throughout the
 * whole application.
 */

/** E-mail of the admin of Top10. */
define("ADMIN_EMAIL", "top10@soft.inima.al");
?>