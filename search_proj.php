<?php
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This script is called from the command line.
 * It gets as a parameter from the command line a search term,
 * then calls the search engine of SourceForge with this search
 * term.  After getting the result (HTML pages), it extracts the
 * project info from it and stores it in a database. 
 */

if ($argc<>2) exit(1);
$search_term = $argv[1];
$query_string = strtr($search_term, " ", "+");
$search_uri = "http://sourceforge.net/search/"
            . "?type=soft&exact=1&q=".$query_string."&offset=";

$page = 1;
$last_page = false;
while (!$last_page)
{
  $offset = ($page - 1) * 25;
  $last_page = get_projects($search_uri.$offset);
  sleep(5);
  $page++;
}

exit(0);

/**
 * Get and store the projects.
 * Return true if last page, otherwise return false;
 */
function get_projects($search_uri)
{
  //debugging output
  print $search_uri."\n";

  //get the html page
  $lines = file($search_uri);
  //$lines = file("test.html");  //debug

  //extract the project info
  $arr_projects = array();
  for ($i=0; $i < sizeof($lines); $i++)
    {
      $line = $lines[$i];
      //print $line;  //debug
      if (ereg('<TD><A HREF="/projects/([^/]+)/">', $line, $regs))
        {
          $proj_id = $regs[1];
          $arr_str = split("</TD><TD>", $line);
          $arr_str_1 = split("<TD>", $arr_str[0]);
          $proj_descr = $arr_str[1];
          $proj_name = strip_tags($arr_str_1[1]);
          $proj_name = str_replace('&nbsp;', '', $proj_name);

          $arr_projects[] = array(
                                  "proj_id" => $proj_id,
                                  "proj_name" => $proj_name,
                                  "proj_descr" => $proj_descr
                                  );
        }
    }
  save_projects($arr_projects);

  //return true;  //debug
  if (sizeof($arr_projects) < 25) 
    return true;
  else 
    return false;
}

/**
 * Save in DB the projects in the array.
 */
function save_projects($arr_projects)
{
  global $search_term;

  $cnn = mysql_connect("localhost", "dasho", "dasho");
  mysql_select_db("sf", $cnn);

  for ($i=0; $i < sizeof($arr_projects); $i++)
    {
      $proj_id = $arr_projects[$i]["proj_id"];
      $proj_name = $arr_projects[$i]["proj_name"];
      $proj_descr = $arr_projects[$i]["proj_descr"];

      $proj_name = str_replace("'", "\\'", $proj_name);
      $proj_descr = str_replace("'", "\\'", $proj_descr);
      $query = "
INSERT INTO proj_list 
SET proj_id = '$proj_id',
    proj_name = '$proj_name',
    proj_descr = '$proj_descr',
    search_term = '$search_term',
    status = 'new'
";
      mysql_query($query, $cnn);
    }

  mysql_close($cnn);
}
?>