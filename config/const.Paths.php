<?php
/* This file is part of phpWebApp. */

/** @package sample-application */
/** */
//constants of the paths in the application
define("WEBAPP_PATH",   UP_PATH."web_app/");
define("TPL",           APP_PATH."templates/");
define("TPL_URL",       APP_URL."templates/");
?>
