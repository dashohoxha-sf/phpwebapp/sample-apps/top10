<?php
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class module extends WebObject
{
  function init()
    {
      WebApp::addSVar("module", "proj_list");
      WebApp::addSVar("username", "guest");
      WebApp::addSVar("preference_list", "");
      WebApp::addSVar("preference_list->unsaved", "false");
    }

  /** Handles event 'proj_details' sent by proj_list. */
  function on_proj_details($event_args)
    {
      WebApp::setSVar("module", "proj_details");
      WebApp::setSVar("menu->selected", "proj_details");

      $proj_id = $event_args["proj_id"];
      WebApp::setSVar("proj_details->proj_id", $proj_id);
    }

  /** Handles event 'add_new_project' sent by list_footer (of proj_list). */
  function on_add_new_project($event_args)
    {
      WebApp::setSVar("module", "edit_project");
      WebApp::setSVar("menu->selected", "proj_details");

      WebApp::setSVar("edit_project->mode", "add");
      WebApp::setSVar("edit_project->proj_id", UNDEFINED);
    }

  /** Handles event 'edit_project' sent by list_footer (of proj_list). */
  function on_edit_project($event_args)
    {
      if (ADMIN <> 'true')  return;

      $proj_id = $event_args["proj_id"];

      WebApp::setSVar("module", "edit_project");
      WebApp::setSVar("menu->selected", "proj_details");

      WebApp::setSVar("edit_project->mode", "edit");
      WebApp::setSVar("edit_project->proj_id", $proj_id);
    }

  /** Handles event 'preference_list' sent by proj_details. */
  function on_preference_list($event_args)
    {
      WebApp::setSVar("menu->selected", "preference_list");
      WebApp::setSVar("module", "preference_list");

      //redirect event to 'preference_list.change_username'
      global $event;
      $event->target = "preference_list";
      $event->name = "change_username";
    }

  function onParse()
    {
      $module = WebApp::getSVar("module");
      switch ($module)
        {
        default:
        case "about":
          $module_file = "about/about.html";
          break;
        case "users":
          $module_file = "admin/users/users.html";
          break;
        case "modify":
          $module_file = "admin/modify/modify.html";
          break;
        case "proj_list":
          $module_file = "proj_list/proj_list.html";
          break;
        case "proj_details":
          $module_file = "proj_details/proj_details.html";
          break;
        case "edit_project":
          $module_file = "edit_project/edit_project.html";
          break;
        case "preference_list":
          $module_file = "preference_list/preference_list.html";
          break;
        }
      WebApp::addVar("module_file", $module_file);    

      WebApp::addGlobalVar("date", date("M d, Y"));
   }
}
?>