<?php
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class menu extends WebObject
{
  function init()
    {
      $this->addSVar("selected", "proj_list");
    }

  function on_select($event_args)
    {
      $selected_tab = $event_args["tab"];
      $this->setSVar("selected", $selected_tab);
      WebApp::setSVar("module", $selected_tab);
    }

  function onRender()
    {
      //add class variables for the tabs
      WebApp::addVars( array(
                             "about"           => "menu-tab",
                             "users"           => "menu-tab",
                             "modify"          => "menu-tab",
                             "proj_list"       => "menu-tab",
                             "proj_details"    => "menu-tab",
                             "preference_list" => "menu-tab",
                             ));

      //set another class for the selected tab
      $selected_tab = $this->getSVar("selected");
      WebApp::addVar($selected_tab, "menu-tab-selected");
    }
}
?>