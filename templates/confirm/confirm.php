<?php
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class confirm extends WebObject
{
  function init()
    {
      $this->addSVar("file", "{{./}}blank.html");
      $this->addSVar("request_id", $_SERVER["QUERY_STRING"]);
      $this->process_request();
    }

  /** Send notification e-mail to admin about an error. */
  function email_admin($error_msg)
    {
      $request_id = $_SERVER["QUERY_STRING"];
      $time = date("Y-m-d H:i:s");
      $remote_addr = $_SERVER["REMOTE_ADDR"];
      $message_body = "
$time,  $remote_addr
request_id = '$request_id'
$error_msg
";
      mail(ADMIN_EMAIL, "Top10: Error", $message_body);
    }

  function process_request()
    {
      $rs = WebApp::openRS("get_request");

      if ($rs->EOF())  
        {
          $error_msg = "No request with this id found; "
            . "maybe this is an error or maybe a malicious request.";
          $this->email_admin($error_msg);
          $this->setSVar("file", "{{./}}error.html");
          return;
        }

      if ($rs->count > 1)
        {
          $error_msg = "More than one requests with this id found; "
            . "maybe this is an error.";
          $this->email_admin($error_msg);
          $this->setSVar("file", "{{./}}error.html");
          return;
        }
    
      $status = $rs->Field('status');
      switch ($status)
        {
        case 'expired':
          $msg = "This request is older than 24 hours and it has expired.";
          WebApp::message($msg);
          break;

        case 'closed':
          $msg = "This request has been already completed.\n"
            . "If you think that there is something wrong,\n"
            . "please e-mail the admin at: '".ADMIN_EMAIL."'.";
          WebApp::message($msg);
          break;

        case 'pending':
          $request = $rs->Fields();
          $this->complete_request($request);
          WebApp::execDBCmd("close_request");
         break;

        default:
          $error_msg = "Unknown status: '$status', there may be "
            . "a bug somewhere in the program.";
          $this->email_admin($error_msg);
          $this->setSVar("file", "{{./}}error.html");
          break;
        }
    }

  function complete_request($request)
    {
      $type = $request["type"];
      switch ($type)
        {
        case 'edit_user_data':
          $username = $request["data"];
          $this->edit_user_data($username);
          break;

        case 'edit_proj_details':
          $username = $request["username"];
          $proj_id = $request["data"];
          $this->edit_project_details($username, $proj_id);
          break;

        case 'delete_comment':
          $username = $request["username"];
          list($proj_id, $comment_id) = explode('/', $request["data"]);
          $this->delete_comment($username, $proj_id, $comment_id);
          break;

        case 'save_pref_list':
          $username = $request["username"];
          $pref_list = $request["data"];
          $this->save_preference_list($username, $pref_list);
          break;

        default:
          $error_msg = "Unknown type: '$type', there may be "
            . "a bug somewhere in the program.";
          $this->email_admin($error_msg);
          $this->setSVar("file", "{{./}}error.html");
          break;
        }
    }

  function edit_user_data($username)
    {
      //display the 'edituser' module
      $this->setSVar("file", "{{TPL}}edituser/edituser.html");
      WebApp::setSVar("edituser->mode", "edit");
      WebApp::setSVar("edituser->username", $username);
    }

  function edit_project_details($username, $proj_id)
    {
      //display the 'edit_project' module
      WebApp::setSVar("username", $username);
      WebApp::setSVar("edit_project->mode", "edit");
      WebApp::setSVar("edit_project->proj_id", $proj_id);
      $this->setSVar("file", "{{TPL}}edit_project/edit_project.html");
    }

  function delete_comment($username, $proj_id, $comment_id)
    {
      //delete the comment
      $ok = WebApp::execDBCmd("delete_comment", compact("comment_id"));
      if ($ok)
        {
          WebApp::message("Comment was deleted successfully.");
        }
      else
        {
          WebApp::message("Something went wrong when deleting the comment.");
        }

      //update the comment count of the project
      $rs = WebApp::openRS("comment_count", compact("proj_id"));
      $nr_comments = $rs->Field("nr_comments");
      $params = compact("proj_id", "nr_comments");
      WebApp::execDBCmd("update_comment_count", $params);

      //display the 'proj_details' module
      WebApp::setSVar("username", $username);
      $this->setSVar("file", "{{TPL}}proj_details/proj_details.html");
      WebApp::setSVar("proj_details->proj_id", $proj_id);
    }

  /**
   * First update the points of the relevant projects, then
   * delete the old preference list and save the new preference list.
   */
  function save_preference_list($username, $pref_list)
    {
      //update the points of each project in which $username is a member
      $this->update_bonus_points($username, $pref_list);

      //for each project in the old list, remove the points of the old list
      $this->remove_old_points($username);

      //for each project in the new list, add the points of the new list
      $this->add_new_points($pref_list);

      //delete the old list
      WebApp::execDBCmd("delete_user_prefs", compact("username"));

      //save the new list
      $this->save_user_prefs($username, $pref_list);

      //display the new preference list
      WebApp::setSVar("username", $username);
      WebApp::setSVar("preference_list", $pref_list);
      WebApp::setSVar("preference_list->unsaved", "false");
      $this->setSVar("file", "{{TPL}}preference_list/preference_list.html");

      //display a confirmation message
      $msg = "Your list of prefered projects was successfully saved.\n"
        . "For confirmation, it is displayed in this page.";
      WebApp::message($msg);
    }

  /**
   * For each project where username is a member,
   * make sure that it gets as many bonus points as the
   * number of projects in the preference list of username
   * (no more than 10). 
   */
  function update_bonus_points($username, $pref_list)
    {
      //get the old bonus points
      $params = array("username" => $username);
      $rs_old_prefs = WebApp::openRS("get_user_prefs", $params);
      $old_bonus_points = $rs_old_prefs->count;
      if ($old_bonus_points > 10)  $old_bonus_points = 10;

      //get new bonus points
      if ($pref_list=='')  $new_bonus_points = 0;
      else
        {
          $arr_pref_list = explode(",", $pref_list);
          $new_bonus_points = sizeof($arr_pref_list);
          if ($new_bonus_points > 10)  $new_bonus_points = 10;
        }
      
      //make the difference
      $diff = $new_bonus_points - $old_bonus_points;

      //get the list of projects in which $username is a member 
      $params = array("username" => $username);
      $rs_user_projects = WebApp::openRS("get_user_projects", $params);

      //for each project in this list update the bonus points
      while (!$rs_user_projects->EOF())
        {
          $proj_id = $rs_user_projects->Field('proj_id');

          //get project points
          $rs = WebApp::openRS("get_proj_points", compact("proj_id"));
          $proj_points = $rs->Field("points");

          //re-calculate the points of the project
          $proj_points += $diff;

          //set them back in DB
          $params = compact("proj_id", "proj_points");
          WebApp::execDBCmd("set_proj_points", $params);

          $rs_user_projects->MoveNext();
        }
    }

  /**
   * For each project in the old list of preferences,
   * remove the points that were assigned to it according
   * to the old list. Only the first 10 projects of the
   * list are updated.
   */
  function remove_old_points($username)
    {
      //get the old list of preferences of $username
      $params = array("username" => $username);
      $rs_old_prefs = WebApp::openRS("get_user_prefs", $params);

      $pos = 0;
      while (!$rs_old_prefs->EOF())
        {
          //ignore any projects after the 10-th one
          if ($pos > 9)  break;

          $proj_id = $rs_old_prefs->Field("proj_id");

          //get project points
          $rs = WebApp::openRS("get_proj_points", compact("proj_id"));
          $proj_points = $rs->Field("points");

          //re-calculate the points of the project
          $proj_points -= (10 - $pos);

          //set them back in DB
          $params = compact("proj_id", "proj_points");
          WebApp::execDBCmd("set_proj_points", $params);

          $rs_old_prefs->MoveNext();
          $pos++;
        }
    }

  /**
   * For each project in the new preference list,
   * add to it some points according to its position
   * in the list. Only the 10 top projects of the
   * list are used.
   */
  function add_new_points($pref_list)
    {
      if ($pref_list=='')  return;

      $arr_pref_list = explode(",", $pref_list);
      for ($i=0; $i < sizeof($arr_pref_list); $i++)
        {
          //ignore any projects after the 10-th one
          if ($i > 9)  break;

          $proj_id = $arr_pref_list[$i];

          //get project points
          $rs = WebApp::openRS("get_proj_points", compact("proj_id"));
          $proj_points = $rs->Field("points");

          //re-calculate the points of the project
          $proj_points += (10 - $i);

          //set them back in DB
          $params = compact("proj_id", "proj_points");
          WebApp::execDBCmd("set_proj_points", $params);
        }
    }

  /**
   * Saves the preference list in the database,
   * along with their position in the list and username.
   */
  function save_user_prefs($username, $pref_list)
    {
      if ($pref_list=='')  return;  //pref list empty, nothing to save

      $arr_insert_values = array();
      $arr_pref_list = explode(",", $pref_list);
      for ($i=0; $i < sizeof($arr_pref_list); $i++)
        {
          $proj_id = $arr_pref_list[$i];
          $arr_insert_values[] = "('$username', '$proj_id', $i)";
        }
      $insert_values = implode(",\n", $arr_insert_values);
      
      if ($insert_values<>'')
        {
          $params = array("insert_values" => $insert_values);
          WebApp::execDBCmd("insert_user_prefs", $params);
        }
    }
}
?>