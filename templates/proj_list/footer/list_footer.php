<?php
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class list_footer extends WebObject
{
  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("proj_list_rs->current_page", $page);
    }

  function on_delete($event_args)
    {
      if (ADMIN <> 'true')  return;

      $selected = $event_args["selected"];
      $params = array("selected_projects" => $this->reformat($selected));
      WebApp::execDBCmd("del_projects", $params);
      WebApp::execDBCmd("del_comments", $params);
      WebApp::execDBCmd("del_members", $params);
      WebApp::execDBCmd("del_evaluations", $params);
      WebApp::setSVar("proj_list_rs->recount", "true");
    }

  function on_approve($event_args)
    {
      if (ADMIN <> 'true')  return;

      $selected = $event_args["selected"];
      $params = array("selected_projects" => $this->reformat($selected));
      WebApp::execDBCmd("approve", $params);
      WebApp::setSVar("proj_list_rs->recount", "true");
    }

  function on_approve_all($event_args)
    {
      if (ADMIN <> 'true')  return;

      WebApp::execDBCmd("approve_all");
      WebApp::setSVar("filter->new", "false");
      WebApp::setSVar("proj_list_rs->recount", "true");
    }

  function reformat($selected_projects)
    {
      $arr = explode(",", $selected_projects);
      $list = "'".implode("', '", $arr)."'";
      return $list;
    }
}
?>