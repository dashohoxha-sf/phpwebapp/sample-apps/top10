// -*-C-*- //tell emacs to use C mode
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function next_rs_page(page)
{
  SendEvent('list_footer','next','page='+page);
}

function add_new_project()
{
  SendEvent("module", "add_new_project");
}

function get_checked_projects()
{
  var projects = document.proj_list.project;

  if (projects.type=="checkbox")  
    {
      //there is only 1 project in the list
      return (projects.checked ? projects.value : '');
    }

  //else
  var checked_projects = new Array;
  for(i=0; projects[i]; i++)
    {
      if (projects[i].checked)
        {
          checked_projects.push(projects[i].value);
        }
    }

  return checked_projects.join();
}

function uncheck_all_projects()
{
  var projects = document.proj_list.project;
  for(i=0; projects[i]; i++)  projects[i].checked = false;
}

function add_to_list()
{
  var selected = get_checked_projects();
  if (selected=="")
    {
      alert("Nothing selected!");
      return;
    }

  //add checked projects to the preference list
  var pref_list = session.getVar("preference_list");
  if (pref_list=="")  pref_list = selected;
  else pref_list += ","+selected;
  session.setVar("preference_list", pref_list);
  //mark that there are unsaved changes
  session.setVar("preference_list->unsaved", "true");

  //display a notification message
  var msg = "Project(s): '" + selected + "' added to the preference list.\n"
    + "Don't forget to save your list before you close the application!";
  alert(msg);

  uncheck_all_projects();
}

/** Used by admin to modify the data of the projects. */
function edit_selected()
{
  var selected = get_checked_projects();
  if (selected=="")
    {
      alert("Nothing selected!");
      return;
    }

  //check that there is selected only one project
  var arr_selected = selected.split(',');
  if (arr_selected.length > 1)
    {
      alert("Please select only one project.");
      return;
    }

  SendEvent("module", "edit_project", "proj_id="+selected);
}

/** Used by admin to delete projects. */
function del_selected()
{
  var selected = get_checked_projects();
  if (selected=="")
    {
      alert("Nothing selected!");
      return;
    }
  var msg = "Are you sure you want to delete "
    + "the project(s): '" + selected + "' ?";
  if (!confirm(msg))  return;
  SendEvent("list_footer", "delete", "selected="+selected);
}

/** Used by admin to approve new projects. */
function approve_selected()
{
  var selected = get_checked_projects();
  if (selected=="")
    {
      alert("Nothing selected!");
      return;
    }
  SendEvent("list_footer", "approve", "selected="+selected);
}

/** Used by admin to approve all the new projects. */
function approve_all()
{
  SendEvent("list_footer", "approve_all");
}
