<?php
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class proj_list extends WebObject
{
  function onRender()
    {
      WebApp::addVar("condition", $this->get_filter_condition());

      $order = WebApp::getSVar("filter->order_by");
      if ($order=='points' or $order=='nr_comments')
        {
          $order .= " DESC";
        }
      WebApp::addVar("order", $order);
      $this->highlight();  //highlight the search term
    }

  function get_filter_condition()
    {
      if (ADMIN=='true')
        {
          $new = WebApp::getSVar("filter->new");
          if ($new=='true')
            {
              //display all the new projects, regardless 
              //of the other filtering conditions 
              $condition = "status='new'";
              return $condition;
            }
        }

      //get the state vars of the WebBox filter
      $language    = WebApp::getSVar("filter->language");
      $license     = WebApp::getSVar("filter->license");
      $search_term = WebApp::getSVar("filter->search_term");
      $search_term = trim($search_term);
      $category    = WebApp::getSVar("filter->category");

      $conditions = array();
      $conditions[] = "status='approved'";
      if ($language <> '')  $conditions[] = "prog_lang='$language'";
      if ($license  <> '')  $conditions[] = "license='$license'";
      if ($category <> '')  $conditions[] = "category='$category'";
      if ($search_term <> '')
        {
          $conditions[] = "(proj_id LIKE '%$search_term%'"
            . " OR proj_name LIKE '%$search_term%'"
            . " OR proj_descr LIKE '%$search_term%')";
        }
      $condition = implode(" AND ", $conditions);

      return $condition;
    }

  /** Highlight the search term. */
  function highlight()
    {
      $term = WebApp::getSVar("filter->search_term");
      if (trim($term)=='')  return;
      $hl_term = "<span style='background: #ddffdd'>$term</span>";

      //get a reference to the recordset
      global $webPage;
      $rs = &$webPage->rs_collection["proj_list_rs"];

      $rs->Open();
      while (!$rs->EOF())
        {
          $proj_id = $rs->Field("proj_id");
          $proj_id = str_replace($term, $hl_term, $proj_id);
          $rs->setFld("proj_id", $proj_id);

          $proj_name = $rs->Field("proj_name");
          $proj_name = str_replace($term, $hl_term, $proj_name);
          $rs->setFld("proj_name", $proj_name);

          $proj_descr = $rs->Field("proj_descr");
          $proj_descr = str_replace($term, $hl_term, $proj_descr);
          $rs->setFld("proj_descr", $proj_descr);

          $rs->MoveNext();
        }
    }
}
?>