<?php
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class filter extends WebObject
{
  function init()
    {
      $this->addSVars( array(
                             "language" => "",
                             "license"  => "",
                             "search_term" => "",
                             "category" => "web application framework",
                             "order_by" => "points"
                             ));
      if (ADMIN=='true')  $this->addSVar("new", "true");
    }

  function on_refresh($event_rags)
    {
      //go to the first page and recount records (because
      //filters might have changed)
      WebApp::setSVar("proj_list_rs->current_page", "1");
      WebApp::setSVar("proj_list_rs->recount", "true");
    }

  function onRender()
    {
      WebApp::addVars( array(
                             'points_selected'        => '',
                             'nr_comments_selected'   => '',
                             'proj_id_selected'       => '',
                             'register_date_selected' => '',
                             ));
      $order_by = $this->getSVar("order_by");
      WebApp::addVar($order_by."_selected", "selected");

      if (ADMIN=='true')
        {
          $new = $this->getSVar("new");
          $checked = ($new=="true" ? "checked" : "");
          WebApp::addVar("new_checked", $checked);
        }
    }
}
?>