// -*-C-*- //tell emacs to use C mode
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


/** 
 * Get a username and save it in session.
 * Send the event 'set_username' if this
 * is the first change (username is 'guest'),
 * or the event 'change_username' otherwise.
 * If there are unsaved changes warn the user
 * about them. 
 */
function change_username()
{
  var username = session.getVar("username");
  if (username=='guest')
    {
      username = prompt("Set a username: ");
      if (username==null)  return;  //canceled
      SendEvent("preference_list", "set_username", "username="+username);
    }
  else
    {
      var unsaved = session.getVar("preference_list->unsaved");
      if (unsaved=='true')
        {
          var msg = "You have unsaved changes"
            + " and they will be lost if you don't save them!";
          if (!confirm(msg))  return;  //canceled
        }
      username = prompt("Set a new username: ");
      if (username==null)  return;  //canceled
      SendEvent("preference_list", "change_username", "username="+username);
    }
}

function proj_details(proj_id)
{
  SendEvent("module", "proj_details", "proj_id="+proj_id);
}

function move_up(proj_id)
{
  SendEvent("preference_list", "move_up", "proj_id="+proj_id); 
}

function remove(proj_id)
{
  SendEvent("preference_list", "delete", "proj_id="+proj_id); 
}

function save_preferences()
{
  var username = session.getVar("username");
  if (username=='guest')
    {
      var msg = "You have not identified yourself yet.\n"
        + "Please identify yourself by clicking on [Change Username].";
      alert(msg);
    }
  else
    {
      SendEvent("preference_list", "save_pref_list");
    }
}
