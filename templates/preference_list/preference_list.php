<?php
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class preference_list extends WebObject
{
  /** 
   * Event 'set_username' is sent when user 'guest' is changed
   * and event 'change_username' is sent for the other changes.
   * We need to distinguish between them because we take different
   * actions in these two cases.
   */
  function on_set_username($event_args)
    {
      $username = $event_args["username"];

      //check that the username exists
      $rs = WebApp::openRS("get_user", $event_args);
      if ($rs->EOF())  //no such user
        {
          $msg = "There is no user with username '$username'.";
          WebApp::message($msg);
          return;
        }

      //set the new username
      WebApp::setSVar("username", $username);

      //merge the DB and session pref lists, 
      //taking care to remove duplications

      $rs_pref_list_db = WebApp::openRS("get_pref_list");
      $arr_pref_list_db = $rs_pref_list_db->getColumn("proj_id");

      $pref_list_sess = WebApp::getSVar("preference_list");
      if ($pref_list_sess=='')  $arr_pref_list_sess = array();
      else $arr_pref_list_sess = explode(",", $pref_list_sess);

      //for each project in the session list, add it to the
      //db list, only if it is not already there
      for ($i=0; $i < sizeof($arr_pref_list_sess); $i++)
        {
          $proj_id = $arr_pref_list_sess[$i];
          if (!in_array($proj_id, $arr_pref_list_db))
            $arr_pref_list_db[] = $proj_id;
        }

      //save the resulting list in the session
      $pref_list = implode(",",  $arr_pref_list_db);
      WebApp::setSVar("preference_list", $pref_list);
    }

  function on_change_username($event_args)
    {
      $username = $event_args["username"];

      //check that the username exists
      $rs = WebApp::openRS("get_user", $event_args);
      if ($rs->EOF())  //no such user
        {
          $msg = "There is no user with username '$username'.";
          WebApp::message($msg);
          return;
        }

      //set the new username
      WebApp::setSVar("username", $username);

      //discard the previous preference list and
      //instead of it place the preference list of 
      //the new user (which is retrived from DB)
      $rs_pref_list = WebApp::openRS("get_pref_list");
      $arr_pref_list = $rs_pref_list->getColumn("proj_id");
      $pref_list = implode(',', $arr_pref_list);
      WebApp::setSVar("preference_list", $pref_list);
      $this->setSVar("unsaved", "false");
    }

  function on_save_pref_list($event_args)
    {
      //make sure that the list doesn't contain any 
      //project in which the username is a member 
      $this->check_pref_list();

      //get a unique id
      $request_id = md5(uniqid(rand()));

      //insert in DB a request with the same id as above
      $ip = $_SERVER["REMOTE_ADDR"];
      $params = compact("request_id", "ip");
      WebApp::execDBCmd("insert_request", $params);

      //send a confirmation request e-mail
      $username = WebApp::getSVar("username");
      $rs = WebApp::openRS("get_user");
      $to = $rs->Field("email");
      $subject = "Top10: Confirmation Required";
      $message = $this->get_message_body($request_id);
      mail($to, $subject, $message);

      //display a confirmation message
      $msg = "A confirmation e-mail has been sent \n"
        . "to $to .\n"
        . "If you don't confirm the changes in 24 hours, \n"
        . "they will be discarded.";
      WebApp::message($msg);

      $this->setSVar("unsaved", "false");
    }

  function on_move_up($event_args)
    {
      $proj_id = $event_args["proj_id"];
      $pref_list = WebApp::getSVar("preference_list");
      $arr_pref_list = explode(",", $pref_list);

      $i = 0;
      while ($arr_pref_list[$i]<>$proj_id) $i++;
      if ($i > 0)
        {
          $arr_pref_list[$i] = $arr_pref_list[$i-1];
          $arr_pref_list[$i-1] = $proj_id;
          //mark that there are unsaved changes
          $this->setSVar("unsaved", "true");
        }

      $pref_list = implode(",", $arr_pref_list);
      WebApp::setSVar("preference_list", $pref_list);
    }

  function on_delete($event_args)
    {
      $proj_id = $event_args["proj_id"];
      $pref_list = WebApp::getSVar("preference_list");
      $arr_pref_list = explode(",", $pref_list);

      //find the item to be removed
      $i = 0;
      while ($arr_pref_list[$i]<>$proj_id)  $i++;

      //shift up all the rest of the array
      while ($i < (sizeof($arr_pref_list) - 1))
        {
          $arr_pref_list[$i] = $arr_pref_list[$i+1];
          $i++;
        }
      //delete the last item of the array
      unset($arr_pref_list[$i]);

      $pref_list = implode(",", $arr_pref_list);
      WebApp::setSVar("preference_list", $pref_list);
      $this->setSVar("unsaved", "true");  //there are unsaved changes
    }

  function onRender()
    {
      $rs_pref_list = $this->get_rs_pref_list();
      global $webPage;
      $webPage->addRecordset($rs_pref_list);
    }

  /**
   * Constructs and returns the recordset that fills
   * the template of the preference list module.
   */
  function get_rs_pref_list()
    {
      $rs_pref_list = new EditableRS("preference_list");

      $preference_list = WebApp::getSVar("preference_list");
      if ($preference_list<>'')
        {
          $arr_pref_list = explode(",", $preference_list);

          //retrive the data of the selected projects from DB
          $pref_list = "'".implode("', '", $arr_pref_list)."'";
          $params = array("pref_list" => $pref_list);
          $proj_list = WebApp::openRS("get_proj_list", $params);

          //build $rs_pref_list with the data of the projects
          for ($i=0; $i < sizeof($arr_pref_list); $i++)
            {
              //calculate points
              $points = 10 - $i;
              if ($points < 0)  $points = 0;

              $proj_id = $arr_pref_list[$i];
              $proj_list->find("proj_id='$proj_id'");
              $project = $proj_list->Fields();
              $project["points"] = $points;

              $rs_pref_list->addRec($project);
            }
        }
      $rs_pref_list->MoveFirst();
      return $rs_pref_list;
    }

  /**
   * Make sure that the list doesn't contain any 
   * projects to which the username is a member,
   * and also it doesn't contain 'phpwebapp', which
   * is excluded from evaluation. Also make sure
   * that the list doesn't contain duplicate projects.
   */
  function check_pref_list()
    {
      $pref_list = WebApp::getSVar("preference_list");
      if ($pref_list=='')  return;

      //get user projects
      $rs = WebApp::openRS("get_user_projects");
      $arr_user_projects = $rs->getColumn("proj_id");

      //check each project and add them to the checked list
      $arr_pref_list = explode(',', $pref_list);
      $checked_pref_list = array();
      for ($i=0; $i < sizeof($arr_pref_list); $i++)
        {
          $proj_id = $arr_pref_list[$i];
          if (in_array($proj_id, $arr_user_projects))
            {
              $msg = "Project '$proj_id' cannot be included in your list,\n"
                . "because you are a member of this project.";
              WebApp::message($msg);
            }
          else if ($proj_id=='phpwebapp')
            {
              $msg = "Project 'phpwebapp' is excluded from evaluation.";
              WebApp::message($msg);
            }
          else if (in_array($proj_id, $checked_pref_list))
            {
              $msg = "Project '$proj_id' more than once in list.";
              WebApp::message($msg);
            }
          else
            {
              $checked_pref_list[] = $proj_id;
            }
        }

      //save them in session
      $pref_list = implode(',', $checked_pref_list);
      WebApp::setSVar("preference_list", $pref_list);
    }

  /**
   * Constructs and returns the body of the message
   * that is sent to the user for confirmation.
   */
  function get_message_body($token)
    {
      //get the recordset of the prefered projects
      //and construct the list (text format) of them
      $rs = $this->get_rs_pref_list();
      if ($rs->EOF())
        {
          //empty, no project in the list
          $proj_list = "  There is no project in the list!";
        }
      else
        {
          while (!$rs->EOF())
            {
              $i++;
              $pts = (11 - $i);
              if ($pts < 0)  $pts = 0;
              $proj_id = $rs->Field("proj_id");
              $proj_name = $rs->Field("proj_name");

              $proj_list .= "  $i.  $proj_id ($pts points)  $proj_name \n";

              $rs->MoveNext();
            }
        }
      
      $username = WebApp::getSVar("username");
      $remote_addr = $_SERVER["REMOTE_ADDR"];
      $top10_site = "http://" . $_SERVER["HTTP_HOST"] . APP_URL;
      $confirmation_uri = $top10_site."confirm.php?".$token;

      $message = "
The site $top10_site received a request from [$remote_addr]
on behalf of user '$username' for saving the list of prefered projects,
which is as follows:

$proj_list

In order to save this list you should confirm this request by opening 
the following URL in 24 hours:

$confirmation_uri

If you don't want to save the list or you received this message by error, 
just ignore and delete it. 

";
      return $message;
    }
}
?>