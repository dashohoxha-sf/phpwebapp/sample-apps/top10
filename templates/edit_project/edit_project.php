<?php
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH."formWebObj.php";

class edit_project extends formWebObj
{
  function init()
    {
      $this->addSVar("mode", "add");  //it can be 'add', 'edit' or 'done'
      $this->addSVar("proj_id", UNDEFINED);
    }

  function on_add($event_args)
    {
      //make sure that the given proj_id is unique
      $proj_id = $event_args["proj_id"];
      $rs = WebApp::openRS("check_proj_id", compact("proj_id"));
      if (!$rs->EOF())  //there is another project with this id
        {
          $msg = "Another project with id '$proj_id' is already "
            . "registered.\n"
            . "Please change the id of the project, or, if you think "
            . "that there is \n"
            . "something wrong, contact the admin at '".ADMIN_EMAIL."'.";
          WebApp::message($msg);
          return;
        }

      //insert the project and save the members
      WebApp::execDBCmd("insert_project", $event_args);
      $members = $event_args["members"];
      $this->save_proj_members($proj_id, $members);

      //change the mode of the webbox
      $this->setSVar("proj_id", $proj_id);
      $this->setSVar("mode", "edit");
    }

  function on_save($event_args)
    {
      WebApp::execDBCmd("update_project", $event_args);

      //save also the members
      $proj_id = $this->getSVar("proj_id");
      $members = $event_args["members"];
      $this->save_proj_members($proj_id, $members);
    }

  function on_done($event_args)
    {
      //save any latest changes
      $this->on_save($event_args);

      //notify admin by e-mail
      extract($event_args);
      $proj_id = $this->getSVar("proj_id");
      $ip = $_SERVER["REMOTE_ADDR"];
      $date = date("Y-m-d");
      $subject = "Top10: New project: '$proj_id'";
      $message = "
          date          = $date
          ip            = $ip
          proj_id       = $proj_id
          proj_name     = $proj_name
          category      = $category
          license       = $license
          prog_lang     = $prog_lang
          url1          = $url1
          url2          = $url2
          proj_descr    = $proj_descr";
      mail(ADMIN_EMAIL, $subject, $message);

      //set the mode of edit_project to 'done'
      $this->setSVar("mode", "done");

      if (ADMIN <> 'true')
        {
          //notification message to the user
          $msg = "Thank you for your submittion.\n" 
            . "This project will be added in the list\n"
            . "after it is approved by the admin.\n"
            . "From now on it can be deleted or modified\n"
            . "only by admin: '".ADMIN_EMAIL."'.";
          WebApp::message($msg);
        }
    }

  function on_send_to_user($event_args)
    {
      if (ADMIN <> 'true')  return;

      $email = $event_args["email"];
      $proj_id = $this->getSVar("proj_id");

      //get a unique id
      $request_id = md5(uniqid(rand()));

      //insert in DB a request with the same id as above
      $ip = $_SERVER["REMOTE_ADDR"];
      $params = compact("request_id", "ip", "email", "proj_id");
      WebApp::execDBCmd("insert_request", $params);

      //send a notification e-mail to the user
      $to = $email;
      $subject = "Top10: Modify Project '$proj_id'";
      $admin_mail = ADMIN_EMAIL;
      $top10_site = "http://" . $_SERVER["HTTP_HOST"] . APP_URL;
      $confirmation_uri = $top10_site."confirm.php?".$request_id;
      $message = "
The admin ($admin_mail) of the site 
$top10_site 
has given you permission to modify the details of the 
project '$proj_id'.  You can modify them by opening 
the following URL in 24 hours:
$confirmation_uri

";
      //mail the message
      mail($to, $subject, $message);

      //display a confirmation message
      $msg = "Project '$proj_id' has been sent for editing\n"
        . " to $email .\n";
      WebApp::message($msg);
    }

  /**
   * Constructs and returns the body of the message
   * that is sent to the user for changing the details of a project.
   */
  function get_message_body($token)
    {    
      $admin_mail = ADMIN_MAIL;
      $username = WebApp::getSVar("username");
      $remote_addr = $_SERVER["REMOTE_ADDR"];
      $top10_site = "http://" . $_SERVER["HTTP_HOST"] . APP_URL;
      $confirmation_uri = $top10_site."confirm.php?".$token;

      $message = "
The admin ($admin_mail) of the site 
$top10_site 
has given you permission to modify the details of the 
project '$proj_id'.  You can modify them by opening 
the following URL in 24 hours:
$confirmation_uri

";
      return $message;
    }

  function onRender()
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          $tpl_vars = array(
                            "proj_id"       => "",
                            "proj_name"     => "",
                            "url1"          => "http://Project Homepage",
                            "url2"          => "http://Development Page",
                            "license"       => "GPL",
                            "prog_lang"     => "PHP",
                            "category"      => "",
                            "members"       => "email1,email2,email3",
                            "proj_descr"    => ""
                            );
          WebApp::addVars($tpl_vars);
        }
      else
        {
          $rs = WebApp::openRS("get_project");
          WebApp::addVars($rs->Fields());
          $rs = WebApp::openRS("get_proj_members");
          $arr_members = $rs->getColumn("username");
          WebApp::addVar("members", implode(",", $arr_members));
        }
    }

  /** Saves the members of a project in the proj_members table. */
  function save_proj_members($proj_id, $members)
    {
      //first delete any existing members
      WebApp::execDBCmd("del_proj_members", compact("proj_id"));

      if (trim($members)=='')  return;  //empty, nothing to save

      //insert the new members
      $arr_insert_values = array();
      $arr_members = explode(",", $members);
      for ($i=0; $i < sizeof($arr_members); $i++)
        {
          $username = trim($arr_members[$i]);
          if ($username=='')  continue;
          $arr_insert_values[] = "('$proj_id', '$username')";
        }
      $insert_values = implode(",\n", $arr_insert_values);
      WebApp::execDBCmd("save_proj_members", compact("insert_values"));
    }
}
?>