// -*-C-*- //tell emacs to use C mode
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function add_project()
{
  var form = document.edit_project;

  if (form.proj_id.value=='')
    {
      alert("Please enter a project id.");
      form.proj_id.focus();
      return;
    }

  //collect all the input in the form
  var event_args = getEventArgs(form); 

  //save the input entered in the form, even if it is not
  //inserted in DB (e.g. because of any error) 
  saveFormData(form);

  SendEvent("edit_project", "add", event_args);
}

function cancel_adding()
{
  select_tab('proj_list');
}

function save_project()
{
  var form = document.edit_project;
  var event_args = getEventArgs(form); 
  SendEvent("edit_project", "save", event_args);
}

function done_editing()
{
  var msg = "You will not be able to modify this project anymore.\n"
    + "If you are creating it for the first time, it will be added\n"
    + "in the list only after it is approved by the admin.";
  if (!confirm(msg))  return;

  if (no_empty_fields())
    {
      var form = document.edit_project;
      var event_args = getEventArgs(form); 
      SendEvent("edit_project", "done", event_args);
    }
}

function no_empty_fields()
{
  var form = document.edit_project;
  var elem;

  for (i=0; form.elements[i]; i++)
    {
      elem = form.elements[i];
      if (elem.name=='url2')  continue;  //this field can be empty
      if (elem.value=='')
        {
          alert("Please fill the field '"+elem.name+"'.");
          eval('form.'+elem.name+'.select()');
          return false;
        }
    }

  return true;
}

function add_new_project()
{
  SendEvent("module", "add_new_project");
}
