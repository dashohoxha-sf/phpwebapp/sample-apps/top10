<?php
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class users extends WebObject
{
  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("users_rs->current_page", $page);
    }

  function on_adduser($event_args)
    {
      WebApp::execDBCmd("adduser", $event_args);
      WebApp::setSVar("users_rs->recount", "true");
    }

  function on_saveuser($event_args)
    {
      WebApp::execDBCmd("saveuser", $event_args);      
    }

  function on_deluser($event_args)
    {
      WebApp::execDBCmd("deluser", $event_args);      
      WebApp::setSVar("users_rs->recount", "true");
    }

  function on_sendforedit($event_args)
    {
      $username = $event_args["username"];
      $email = $event_args["email"];

      //get a unique id
      $request_id = md5(uniqid(rand()));

      //insert in DB a request with the same id as above
      $ip = $_SERVER["REMOTE_ADDR"];
      $params = compact("request_id", "ip", "email", "username");
      WebApp::execDBCmd("insert_request", $params);

      //send a notification e-mail to the user
      $to = $email;
      $subject = "Top10: Modify User '$username'";
      $admin_mail = ADMIN_EMAIL;
      $top10_site = "http://" . $_SERVER["HTTP_HOST"] . APP_URL;
      $confirmation_uri = $top10_site."confirm.php?".$request_id;
      $message = "
The admin ($admin_mail) of the site 
$top10_site 
has given you permission to modify the data of the 
user '$username'.  You can modify them by opening 
the following URL in 24 hours:
$confirmation_uri

";
      //mail the message
      mail($to, $subject, $message);

      //display a confirmation message
      $msg = "User '$username' has been sent for editing\n"
        . " to $email .\n";
      WebApp::message($msg);
    }

  function onRender()
    {
      WebApp::addVar('user_filter', $this->get_user_filter());
    }

  /**
   * Construct an SQL filter (condition) 
   * based on the variables of the user_filter.
   */
  function get_user_filter()
    {
      //get state vars of user_filter
      $username = WebApp::getSVar('user_filter->username');
      $realname = WebApp::getSVar('user_filter->realname');
      $email    = WebApp::getSVar('user_filter->email');
      $status   = WebApp::getSVar('user_filter->status');

      $arr_conditions = array();
      if ($username<>'')  $arr_conditions[] = "username LIKE '%$username%'"; 
      if ($realname<>'')  $arr_conditions[] = "realname LIKE '%$realname%'"; 
      if ($email<>'')     $arr_conditions[] = "email LIKE '%$email%'"; 
      if ($status<>'')    $arr_conditions[] = "status LIKE '%$status%'";
      $user_filter = implode(' AND ', $arr_conditions);
      if ($user_filter=='')  $user_filter = '1=1';

      return $user_filter;
    }
}
?>