// -*-C-*- //tell emacs to use C mode
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function saveuser(username)
{
  var form = document.users;
  var realname = eval("form."+username+"_realname.value");
  var email    = eval("form."+username+"_email.value");
  var homepage = eval("form."+username+"_homepage.value");
  var status   = eval("form."+username+"_status.value");

  var event_args = "username=" + username + ";" 
    + "realname=" + realname + ";"
    + "email=" + email + ";"
    + "homepage=" + homepage + ";"
    + "status=" + status;

  SendEvent("users", "saveuser", event_args);
}

function deluser(username)
{
  SendEvent("users", "deluser", "username="+username);
}

function email2user(username)
{
  var form = document.users;
  var email = eval("form."+username+"_email.value");
  var email = prompt("Send for editing to this e-mail:", email);
  if (email==null)  return;  //canceled

  
  var event_args = "username=" + username + ";" 
    + "email=" + email + ";";

  SendEvent("users", "sendforedit", event_args);
}

