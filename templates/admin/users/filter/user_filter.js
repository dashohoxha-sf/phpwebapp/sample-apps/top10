// -*-C-*- //tell emacs to use C mode
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function set_filter_var(txtbox)
{
  var var_name = "user_filter->" + txtbox.name;
  session.setVar(var_name, txtbox.value);
}

function set_status(listbox)
{
  var var_name = "user_filter->" + listbox.name;
  var var_value = listbox.options[listbox.selectedIndex].value;
  session.setVar(var_name, var_value);
}

function refresh()
{
  SendEvent('user_filter','refresh');
}
