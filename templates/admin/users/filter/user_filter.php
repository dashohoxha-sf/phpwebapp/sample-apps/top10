<?php
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class user_filter extends WebObject
{
  function init()
    {
      $this->addSVar('username', '');
      $this->addSVar('realname', '');
      $this->addSVar('email', '');
      $this->addSVar('status', '');
      $this->addSVar('condition', '(1=1)');
    }

  function on_refresh()
    {
      //get filter variables
      $username = $this->getSVar('username');
      $realname = $this->getSVar('realname');
      $email = $this->getSVar('email');
      $status = $this->getSVar('status');

      //build the filter condition
      $arr_conditions = array();
      if ($username<>'') $arr_conditions[] = "username like '%$username%'";
      if ($realname<>'') $arr_conditions[] = "realname like '%$realname%'";
      if ($email<>'')    $arr_conditions[] = "email like '%$email%'";
      if ($status<>'')   $arr_conditions[] = "status='$status'";

      $condition = implode(' AND ', $arr_conditions);
      if ($condition=='')  $condition = '(1=1)';

      //save it
      $this->setSVar('condition', $condition);
    }

  function onRender()
    {
      //build the recordset used by the 'status' listbox
      $rs = new EditableRS('status');
      $rs->addRec(array('id'=>'private', 'label'=>'private'));
      $rs->addRec(array('id'=>'public', 'label'=>'public'));
      //add it to the page
      global $webPage;
      $webPage->addRecordset($rs);
    }
}
?>