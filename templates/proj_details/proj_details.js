// -*-C-*- //tell emacs to use C mode

function change_project()
{
  var proj_id = document.change.proj_id.value;
  SendEvent("proj_details", "set_project", "proj_id="+proj_id);
}

function set_project(proj_id)
{
  SendEvent("proj_details", "set_project", "proj_id="+proj_id);
}

function preference_list(username)
{
  SendEvent("module", "preference_list", "username="+username);
}
