<?php
class comments extends WebObject
{
  function on_add_comment($event_args)
    {
      $params["username"] = $event_args["username"];
      $params["title"] = $event_args["title"];
      $comment = $event_args["comment"];
      $allowed_tags = "<b><i><a><br><p><ol><ul><li><hr><pre><xmp>";
      $params["comment"] = strip_tags($comment, $allowed_tags);
      $params["date"] = date("Y-m-d");
      $params["ip"] = $_SERVER["REMOTE_ADDR"];
      //add the comment record
      WebApp::execDBCmd("insert_comment", $params);

      $this->update_comment_count();
    }

  function update_comment_count()
    {
      //update the comment count of the project
      $rs = WebApp::openRS("comment_list");
      $nr_comments = $rs->count;
      WebApp::execDBCmd("update_comment_count", compact("nr_comments"));
    }

  function on_del_comment($event_args)
    {
      if (ADMIN=='true')
        {
          WebApp::execDBCmd("delete_comment", $event_args);
          $this->update_comment_count();
          return;
        }

      //if not admin, then send a confirmation request 
      //to the author of the comment

      //get the comment details
      $comment_id = $event_args["comment_id"];
      $rs = WebApp::openRS("get_comment", compact("comment_id"));
      $username = $rs->Field("username");
      $proj_id = $rs->Field("proj_id");
      $title = $rs->Field("title");
      $comment = $rs->Field("comment");
      $date = $rs->Field("date");

      //get the email of the user
      $rs = WebApp::openRS("get_user", compact("username"));
      if ($rs->EOF())  //no such user
        {
          //show an error message to the user
          WebApp::message("User '$username' does not exist.");
          //send a notification message to admin
          $ip = $_SERVER["REMOTE_ADDR"];
          $message = "
Somebody from [$ip] tried to delete the following 
comment from project '$proj_id':
 
Title: $title
Comment: $comment
";
          $subject = "Top10: Delete Comment";
          mail(ADMIN_EMAIL, $subject, $message);
          return;
        }
      $to = $rs->Field("email");

      //get a unique id
      $request_id = md5(uniqid(rand()));

      //insert in DB a request with the same id as above
      $ip = $_SERVER["REMOTE_ADDR"];
      $params = compact("request_id", "ip", "username", "comment_id");
      WebApp::execDBCmd("insert_request", $params);

      //send a notification e-mail to the user
      $subject = "Top10: Delete Comment";
      $top10_site = "http://" . $_SERVER["HTTP_HOST"] . APP_URL;
      $confirmation_uri = $top10_site."confirm.php?".$request_id;
      $message = "
The site $top10_site received a request from [$ip]
for deleting the following comment that you have made for the 
project '$proj_id':
 
Title: $title
Comment: $comment

In order to delete this comment you should confirm this request by opening 
the following URL in 24 hours:

$confirmation_uri

If you changed your mind and you don't want to delete the comment, 
just ignore and delete this message. 

";
      //mail the message
      mail($to, $subject, $message);

      //display a confirmation message
      $msg = "A confirmation e-mail has been sent \n"
        . "to $to .\n"
        . "If you don't confirm the delete request in 24 hours, \n"
        . "it will be discarded.";
      WebApp::message($msg);
    }
}
?>