// -*-C-*- //tell emacs to use C mode

function send_comment()
{
  var form = document.new_comment;
  var username = form.username.value;
  var title = form.title.value;
  var comment = form.comment.value;

  if (comment=='')
    {
      alert("Please enter a comment!");
      form.comment.focus();
      return;
    }

  if (title=='')
    {
      var msg = "You did not specify a title. Send the comment anyway?";
      if (!confirm(msg))
        {
          form.title.focus();
          return;
        }
    }

  var event_args = "username=" + username + ";"
    + "title=" + title + ";"
    + "comment=" + comment;

  SendEvent("comments", "add_comment", event_args);
}

function del_comment(comment_id)
{
  SendEvent("comments", "del_comment", "comment_id="+comment_id);
}
