<?php
/*
This file  is part of Top10.   Top10 is a web  application for ranking
and evaluating free software projects.

Copyright (C) 2003, 2004 Dashamir Hoxha, dashohoxha@users.sf.net

Top10 is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

Top10 is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with Top10; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class edituser extends WebObject
{
  function init()
    {
      $this->addSVar("mode", "add");  //'add'|'edit'
      $this->addSVar("username", UNDEFINED);
    }

  function on_add($event_args)
    {
      WebApp::execDBCmd("adduser", $event_args);
      WebApp::setSVar("users_rs->recount", "true");
    }

  function on_save($event_args)
    {
      WebApp::execDBCmd("saveuserdata", $event_args);      
    }

  function onRender()
    {
      $mode = $this->getSVar("mode");
      if ($mode=='edit')
        {
          $rs = WebApp::openRS("getuser");
          WebApp::addVars($rs->Fields());
        }
      else
        {
          WebApp::addVars(array("realname" => "",
                                "email"    => "",
                                "homepage" => "",
                                "status"   => "private"));
        }
    }
}
?>