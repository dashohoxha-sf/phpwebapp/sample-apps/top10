
+ Add to project details URL1, URL2, etc.
 
+ When the admin sends the project for edit to another person
  he should supply his e-mail address (instead of the username).

+ When editing a project, select the license, the language and
  the category with a listbox.

+ Improve the look of the filters.

+ Add an interface where the user can change his data (except username).

- Add a filter for users, remove a user (?) etc.  

- At the preference list display also the user details (name, email etc.)
  if the user wants to make them public (status=public).

- Each developper of a new project is automatically registered as an 
  evaluator (by sending them a confirmation e-mail, like an invitation).

- A user that is not registered yet as an evaluator can do so at the
  time that he tries to save his preference list.  His registration should
  be approved by admin, and his preference list is saved only after he
  is approved by admin and the list confirmed (by email) by the user.

---------------

- Add a hierarchical menu for selecting the category.

- Add a module for the admin where he can change the categories,
  languages, his password, etc.  


