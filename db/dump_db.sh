#!/bin/bash

read -p "Enter database name: " -a dbname
read -p "Enter your username: " -a username
read -p "Enter filename: " -a filename
echo "$ mysqldump -u $username -p -c -e --add-drop-table $dbname > $filename"
mysqldump -u $username -p -c -e --add-drop-table $dbname > $filename
