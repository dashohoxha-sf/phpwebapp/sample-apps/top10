
-- Change status to 'expired' for all pending requests 
-- that are older than 24 hours.
UPDATE requests
SET status = 'expired'
WHERE status = 'pending'
  AND DATE_ADD(time, INTERVAL 24 HOUR) < NOW()
;


-- Move all the requests older than 1 month from the table
-- requests to the table old_requests.

-- INSERT 
--   INTO old_requests (request_id, time, username, ip, type, data, status)
--   SELECT request_id, time, username, ip, type, data, status
--   FROM requests
--   WHERE status = 'expired'
--     AND DATE_ADD(time, INTERVAL 1 MONTH) < NOW()
-- ;
-- DELETE  FROM requests
-- WHERE status = 'expired'
--   AND DATE_ADD(time, INTERVAL 1 MONTH) < NOW()
-- ;


